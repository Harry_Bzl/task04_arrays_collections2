package Collections2;

public enum Command {
    ACTION1 ("It was action number 1"),
    ACTION2 ("It was action number 2"),
    ACTION3 ("It was action number 3"),
    ACTION4 ("It was action number 4"),
    EXIT ("It was Exit");

    private  String massage;
    Command (String message){
        this.massage = message;
    }

    public String getMassage() {
        return massage;
    }

    public void printMessage () {
        System.out.println(getMassage());
    }
}
