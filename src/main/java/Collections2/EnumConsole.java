package Collections2;

import java.util.Scanner;

public class EnumConsole {
    public static void main(String[] args) {
        System.out.println("Choose the command: ");
        System.out.println(Command.ACTION1);
        System.out.println(Command.ACTION2);
        System.out.println(Command.ACTION3);
        System.out.println(Command.ACTION4);
        System.out.println(Command.EXIT);
        System.out.println("Type ansver: ");

         Scanner in = new Scanner(System.in);
         String command = in.nextLine().toUpperCase();

         Command.valueOf(command).printMessage();
    }


}
