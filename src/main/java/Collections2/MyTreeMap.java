package Collections2;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MyTreeMap <Key extends Comparable<Key>,Value> {
    private Node root;

    private  class Node {
        private Key key;
        private Value val;
        private Node left, right;
        private int N;
        /**
         * constructor for Node inner class
         * @param key the key
         * @param val the value
         * @param N number of bundles
         */
        public Node (Key key, Value val, int N){
            this.key =key;
            this.val = val;
            this.N = N;
        }
    }

    public int size () {
        return size(root);
    }

    private int size (Node x){
        if (x==null){
            return 0;
        } else {
            return x.N;
        }
    }

    public Value get (Key key) {
        return get(root, key);
    }

    /**
     *
     * @param x current node
     * @param key
     * @return value by key or null if absent
     */
    private Value get (Node x, Key key){
        if (x==null) return null;
        int cmp = key.compareTo(x.key);
        if (cmp < 0) return get(x.left, key);
        else if (cmp >0) return  get(x.right, key);
        else return  x.val;
    }

    public void put (Key key, Value val){
        root = put (root, key, val);
    }

    /**
     * if key is in tree value replace that value
     * else - add new bundle
     * @param x node
     * @param key the key
     * @param val the value
     * @return replaced or new node
     */
    private Node put (Node x, Key key, Value val){
        if (x==null) return new Node(key, val, 1);
        int cmp = key.compareTo(x.key);
        if (cmp < 0) x.left = put (x.left, key, val);
        else if (cmp > 0) x.right = put(x.right, key, val);
        else x.val = val;
        x.N = size(x.left) + size(x.right) + 1;
        return x;
    }

    public Key min () {
        return min(root).key;
    }

    private  Node min (Node x){
        if (x.left==null) return x;
        return min(x.left);
    }

    public Key max () {
        return max(root).key;
    }

    private  Node max (Node x){
        if (x.right==null) return x;
        return max(x.right);
    }

    public void deleteMin (){
        deleteMin(root);
    }

    private Node deleteMin (Node x){
        if (x.left == null) return x.right;
        x.left = deleteMin(x.left);
        x.N = size(x.left) + size(x.right) + 1;
        return x;
    }

    public void delete (Key key){
        root = delete (root, key);
    }

    private Node delete (Node x, Key key){
        if (x==null) return null;
        int cmp = key.compareTo(x.key);
        if (cmp<0) x.left = delete(x.left, key);
        else if (cmp>0) x.right = delete(x.right, key);
        else {
            if (x.right == null) return x.left;
            if (x.left == null) return x.right;
            Node t = x;
            x = min(t.right);
            x.right = deleteMin(t.right);
            x.left = t.left;
        }
        x.N = size(x.left)+size(x.right)+1;
        return x;
    }

    public void print (Node x){
        if (x == null){
            return;
        }
        print(x.left);
        System.out.println(x.key);
        print(x.right);
    }

    public Iterable <Key> keys (){
        return keys(min(), max());
    }

    public Iterable <Key> keys (Key lower, Key hier){
        ArrayList<Key> resultArray = new ArrayList<Key>();
        keys(root, resultArray, lower, hier);
        return resultArray;
    }

    private void keys (Node x, ArrayList <Key> resultArray, Key lower, Key hier){
        if (x==null) return;
        int cmplo = lower.compareTo(x.key);
        int cmphi = hier.compareTo(x.key);
        if (cmplo < 0) keys(x.left, resultArray, lower, hier);
        if (cmplo <= 0 && cmphi >= 0) resultArray.add(x.key);
        if (cmphi > 0) keys(x.right, resultArray, lower, hier);

    }
}
