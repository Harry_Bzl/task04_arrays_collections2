package Collections2;

import java.util.Random;

public class TestTree {
    public static void main(String[] args) {
        MyTreeMap <Integer,String> map = new MyTreeMap<Integer, String>();
        Random rand = new Random ();
        for (int i = 0; i < 20; i++) {
            Integer n = rand.nextInt(10);
            map.put(n, "Value "+n);
        }

        System.out.println("Created tree keys is: ");
        for (Integer i: map.keys()){
            System.out.print(i + ", ");
        }
        System.out.println();

        System.out.println("Value of key 5 is " + map.get(5));

        System.out.println("Delete key 5");
        map.delete(5);
        System.out.println("Created tree keys is: ");
        for (Integer i: map.keys()){
            System.out.print(i + ", ");
        }

    }
}
